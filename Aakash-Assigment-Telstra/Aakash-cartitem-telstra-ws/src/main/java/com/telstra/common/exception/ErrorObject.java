package com.telstra.common.exception;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ErrorObject implements Serializable {
 
	private static final long serialVersionUID = 1L;

	private String code;
	private String message;
	private String field;
	private Map<String, String> dynamicProperties;

	public ErrorObject() {
		super();
	}

	public ErrorObject(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public ErrorObject(String code, String message, String field) {
		super();
		this.code = code;
		this.message = message;
		this.field = field;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public Map<String, String> getDynamicProperties() {
		return dynamicProperties;
	}

	public void setDynamicProperties(Map<String, String> dynamicProperties) {
		this.dynamicProperties = dynamicProperties;
	}

}
