package com.telstra.common.exception;

import java.util.ArrayList;
import java.util.List;
 

/**
 * This class denotes an exception to be thrown, if data already exists in the system (Duplicate request)
 *
 */
public class ServiceFailureException extends Exception {

	private static final long serialVersionUID = 1L;

	private List<ErrorObject> errors;

	public ServiceFailureException(String code, String message) {
		super(message);
		ErrorObject errObject = new ErrorObject(code, message);
		this.addError(errObject);
	}

	public ServiceFailureException(String message, List<ErrorObject> errors) {
		super(message);
		this.setErrors(errors);
	}
	
	public ServiceFailureException(String message, ErrorObject error) {
		super(message);
		this.addError(error);
	}

	public ServiceFailureException(String message) {
		super(message);
	}
	public List<ErrorObject> getErrors() {
		return errors;
	}

	public void setErrors(List<ErrorObject> errors) {
		this.errors = errors;
	}

	public void addError(ErrorObject error) {
		if (this.errors == null) {
			this.errors = new ArrayList<>();
		}
		this.errors.add(error);
	}
}
