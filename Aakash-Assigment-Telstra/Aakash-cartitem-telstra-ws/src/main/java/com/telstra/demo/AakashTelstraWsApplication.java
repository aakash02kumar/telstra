package com.telstra.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages="com")
public class AakashTelstraWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AakashTelstraWsApplication.class, args);
	}
}
 