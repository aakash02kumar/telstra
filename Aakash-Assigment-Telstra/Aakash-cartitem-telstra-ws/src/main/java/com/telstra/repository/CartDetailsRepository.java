package com.telstra.repository;

import javax.persistence.PersistenceContext;

import org.springframework.data.repository.CrudRepository;

import com.telstra.entity.CartItemEntity;

@PersistenceContext(name = "testd")
public interface CartDetailsRepository extends CrudRepository<CartItemEntity, Long> {
	public CartItemEntity findByUser(String user);

}
