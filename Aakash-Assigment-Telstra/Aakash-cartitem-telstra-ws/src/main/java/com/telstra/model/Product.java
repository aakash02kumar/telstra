package com.telstra.model;
 
public class Product {
String productId;
String productName;
String category;
String productModel;
Double price;
Integer availableQuantity;

public String getProductId() {
	return productId;
}
public void setProductId(String productId) {
	this.productId = productId;
}
public String getCategory() {
	return category;
}
public void setCategory(String category) {
	this.category = category;
}
public String getProductName() {
	return productName;
}
public void setProductName(String productName) {
	this.productName = productName;
}
public String getProductModel() {
	return productModel;
}
public void setProductModel(String productModel) {
	this.productModel = productModel;
}
public Double getPrice() {
	return price;
}
public void setPrice(Double price) {
	this.price = price;
}
public Integer getAvailableQuantity() {
	return availableQuantity;
}
public void setAvailableQuantity(Integer availableQuantity) {
	this.availableQuantity = availableQuantity;
}
@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + availableQuantity;
	result = prime * result + ((category == null) ? 0 : category.hashCode());
	long temp;
	temp = Double.doubleToLongBits(price);
	result = prime * result + (int) (temp ^ (temp >>> 32));
	result = prime * result + ((productId == null) ? 0 : productId.hashCode());
	result = prime * result + ((productModel == null) ? 0 : productModel.hashCode());
	result = prime * result + ((productName == null) ? 0 : productName.hashCode());
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Product other = (Product) obj;
	if (availableQuantity != other.availableQuantity)
		return false;
	if (category == null) {
		if (other.category != null)
			return false;
	} else if (!category.equals(other.category))
		return false;
	if (Double.doubleToLongBits(price) != Double.doubleToLongBits(other.price))
		return false;
	if (productId == null) {
		if (other.productId != null)
			return false;
	} else if (!productId.equals(other.productId))
		return false;
	if (productModel == null) {
		if (other.productModel != null)
			return false;
	} else if (!productModel.equals(other.productModel))
		return false;
	if (productName == null) {
		if (other.productName != null)
			return false;
	} else if (!productName.equals(other.productName))
		return false;
	return true;
}

}
