package com.telstra.model;

public class CartItem {
String productId;
String productName;
Double amount;
Integer quantity;
String user;
 
public String getProductId() {
	return productId;
}
public void setProductId(String productId) {
	this.productId = productId;
}
public String getProductName() {
	return productName;
}
public void setProductName(String productName) {
	this.productName = productName;
}
public Double getAmount() {
	return amount;
}
public void setAmount(Double amount) {
	this.amount = amount;
}
public Integer getQuantity() {
	return quantity;
}
public void setQuantity(Integer quantity) {
	this.quantity = quantity;
}
public String getUser() {
	return user;
}
public void setUser(String user) {
	this.user = user;
}


}
