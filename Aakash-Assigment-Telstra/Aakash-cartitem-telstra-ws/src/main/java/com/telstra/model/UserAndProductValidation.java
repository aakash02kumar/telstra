package com.telstra.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.telstra.common.exception.ServiceFailureException;
import com.telstra.service.ProductDetailsExternaService;
import com.telstra.service.CartService;

@Component
public class UserAndProductValidation {
	@Autowired
	private ProductDetailsExternaService productDetailsExternaService;

	@Autowired
	private CartService cartService;

	
	List<String> users = new ArrayList<>();

	public boolean isValid(String user) {
		users.add("John");
		users.add("Mario");
		users.add("Paulo");
		users.add("Aakash");
		users.add("Steve");
		users.add("Telstra");
		users.add("Smith");

		if (users.contains(user))
			return true;
		else
			return false;

	}

	public void UpdateCartItem(String user, CartItem cart) {
		try {
			
			Product availableProducts	=productDetailsExternaService.getAllProductDetail(cart);
			if((availableProducts.getProductId().equals(cart.getProductId()) && (availableProducts.getAvailableQuantity()>cart.getQuantity()))) {
				try {
					cartService.updateCartDetails(cart);
					System.out.println("cart details updated...");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else {
				System.out.println("Product is not available with given quantity.."); 
				
			}
		} catch (ServiceFailureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
