package com.telstra.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.telstra.entity.CartItemEntity;
import com.telstra.model.CartItem;
import com.telstra.repository.CartDetailsRepository;
import com.telstra.service.CartService;

@Component
public class CartServiceImpl implements CartService {

	@Autowired
	CartDetailsRepository cartDetailsRepository;

	CartItemEntity cartItemEntity;

	@Override
	public CartItem fetchUserCartDetails(String user) throws Exception {
		CartItem cartItem = new CartItem();

		if (user != null) {
			cartItemEntity = cartDetailsRepository.findByUser(user);
			if (cartItemEntity != null) {
				cartItem.setUser(cartItemEntity.getUser());
				cartItem.setProductId(cartItemEntity.getProductId());
				cartItem.setProductName(cartItemEntity.getProductName());
				cartItem.setAmount(cartItemEntity.getPrice());
				cartItem.setQuantity(cartItemEntity.getAvailableQuantity());
			}else {
				cartItem.setUser("user " +user +" Not available in db");
				cartItem.setProductId("");
				cartItem.setProductName("");
				cartItem.setAmount(0.0);
				cartItem.setQuantity(0);
			}

		}
		return cartItem;
	}

	@Override
	public String updateCartDetails(CartItem prod) throws Exception {
		cartItemEntity = new CartItemEntity();
		cartItemEntity.setUser(prod.getUser());
		cartItemEntity.setProductId(prod.getProductId());
		cartItemEntity.setProductName(prod.getProductName());
		cartItemEntity.setPrice(prod.getAmount());
		cartItemEntity.setAvailableQuantity(prod.getQuantity());
		if (!prod.toString().isEmpty()) {
			cartItemEntity = cartDetailsRepository.save(cartItemEntity);
		}

		return "data updated successfull";
	}

}
