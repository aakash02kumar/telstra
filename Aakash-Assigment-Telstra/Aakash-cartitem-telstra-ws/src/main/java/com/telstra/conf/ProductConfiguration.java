package com.telstra.conf;
 
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

@Configuration
@EnableJpaRepositories(basePackages = "com.telstra.repository", entityManagerFactoryRef = "mysqlEntityManager")
public class ProductConfiguration {

	@Bean(name = "datasource")
	@Primary
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource batchDetailsDatasource() {
		
	
		DriverManagerDataSource dataSource=new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:1130/testdb");
		dataSource.setPassword("root");
		dataSource.setUsername("root");
		return dataSource;
	}

	@Bean(name = "mysqlEntityManager")
	@Primary
	public LocalContainerEntityManagerFactoryBean mysqlEntityManager(EntityManagerFactoryBuilder builder) {
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put("hibernate.show-sql", "true");
		properties.put("hibernate.hbm2ddl.auto", "update");
		properties.put("spring.jpa.properties.hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		return builder.dataSource(batchDetailsDatasource()).packages("com.telstra.entity").properties(properties)
				.persistenceUnit("testd").build();

	}

}