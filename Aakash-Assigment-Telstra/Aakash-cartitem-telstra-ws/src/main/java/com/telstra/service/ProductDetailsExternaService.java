package com.telstra.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.telstra.common.exception.ServiceFailureException;
import com.telstra.model.CartItem;
import com.telstra.model.Product;

@Component
public class ProductDetailsExternaService {

	@Value("${product.url}")
	private String productFetchUrl;

	public Product getAllProductDetail(CartItem cart) throws ServiceFailureException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("prodid", cart.getProductId());
	
		RestTemplate restTemplate = new RestTemplate();
		Product prod= restTemplate.getForObject(productFetchUrl, Product.class,params);
		return prod;
	}

}
