package com.telstra.service;

import org.springframework.stereotype.Service;

import com.telstra.model.CartItem;

@Service
public interface CartService {

	public CartItem fetchUserCartDetails(String user) throws Exception;
	public String updateCartDetails(CartItem cart) throws Exception;

}
