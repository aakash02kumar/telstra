package com.telstra.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.common.exception.DataNotFoundException;
import com.telstra.common.exception.ServiceFailureException;
import com.telstra.model.CartItem;
import com.telstra.model.UserAndProductValidation;
import com.telstra.service.CartService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

@RestController
@RequestMapping("/rest/v1/users")
@Api(value = "Product Query Controller")
public class ProductController {

	@Autowired
	private CartService cartService;
 
	@Autowired
	private UserAndProductValidation userAndProductValidation;

	@RequestMapping(value = "/GET",method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Fetch Product details", notes = "Endpoint to fetch Product details for given product id", authorizations = {
			@Authorization(value = "oauth2") })
	@ApiResponses({
			@ApiResponse(code = 200, message = "Successfully retrieved the Product details", response = CartItem.class),
			@ApiResponse(code = 400, message = "Missing required information in the request"),
			@ApiResponse(code = 500, message = "Internal error occured") })
	public CartItem getProductDetails(@RequestParam(value = "user", required = true) String user)
			throws DataNotFoundException, ServiceFailureException, Exception {

		return cartService.fetchUserCartDetails(user);
	}

	@RequestMapping(value = "/{user}/cart", method = RequestMethod.PUT)
	public ResponseEntity<CartItem> updateUser(@PathVariable("user") String user, @RequestBody CartItem cart)
			throws Exception {
		System.out.println("Updating product for user " + user);

		if (userAndProductValidation.isValid(user)) {
			userAndProductValidation.UpdateCartItem(user, cart);
		}
		return new ResponseEntity<CartItem>(cart, HttpStatus.OK);
	}
}
