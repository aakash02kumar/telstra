package com.telstra.repository;

import java.util.List;

import javax.persistence.PersistenceContext;

import org.springframework.data.repository.CrudRepository;

import com.telstra.entity.ProductsDetailEntity;

@PersistenceContext(name = "testd")
public interface ProductDetailsRepository extends CrudRepository<ProductsDetailEntity, String> {
	List<ProductsDetailEntity> findAll();
	ProductsDetailEntity findByProductId(String prodid);
}

