package com.telstra.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.common.exception.DataNotFoundException;
import com.telstra.common.exception.ServiceFailureException;
import com.telstra.model.Product;
import com.telstra.service.ProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

@RestController
@RequestMapping("/v1")
@Api(value = "Product Query Controller")
public class ProductControllerExternal {

	@Autowired
	private ProductService productService;

	@RequestMapping(value = "/products", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Fetch Product details", notes = "Endpoint to fetch Product details for given product id", authorizations = {
			@Authorization(value = "oauth2") })
	@ApiResponses({
			@ApiResponse(code = 200, message = "Successfully retrieved the Product details", response = Product.class),
			@ApiResponse(code = 400, message = "Missing required information in the request"),
			@ApiResponse(code = 500, message = "Internal error occured") })
	public ResponseEntity<Product> getProductDetailsExternal(@RequestParam(value = "prodid", required = true) String prodid)
			throws DataNotFoundException, ServiceFailureException, Exception {
		Product prod = productService.fetchProductDetails(prodid);
		System.out.println(prod.toString());
		return new ResponseEntity<Product>(prod, HttpStatus.OK);
	}
	@RequestMapping(value = "/allProducts", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Fetch Product details", notes = "Endpoint to fetch all Product details", authorizations = {
			@Authorization(value = "oauth2") })
	@ApiResponses({
			@ApiResponse(code = 200, message = "Successfully retrieved the Product details", response = Product.class),
			@ApiResponse(code = 400, message = "Missing required information in the request"),
			@ApiResponse(code = 500, message = "Internal error occured") })
	public ResponseEntity<List<Product>> getAllProductDetailsExternal()
			throws DataNotFoundException, ServiceFailureException, Exception {
		List<Product> prod = productService.findAll();
		System.out.println(prod.toString());
		return new ResponseEntity<List<Product>>(prod, HttpStatus.OK);
	}
}
