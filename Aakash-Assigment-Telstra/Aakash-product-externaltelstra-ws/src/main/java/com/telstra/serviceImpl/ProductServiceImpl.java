package com.telstra.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.telstra.entity.ProductsDetailEntity;
import com.telstra.model.Product;
import com.telstra.repository.ProductDetailsRepository;
import com.telstra.service.ProductService;

@Component
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductDetailsRepository productDetailsRepository;

	ProductsDetailEntity productsDetailEntity1;
	List<ProductsDetailEntity> productsDetailEntity2;

	@Override
	public Product fetchProductDetails(String prodid) throws Exception {

		productsDetailEntity1 = productDetailsRepository.findByProductId(prodid);

		return mapEntityToDomain(productsDetailEntity1);
	}

	private Product mapEntityToDomain(ProductsDetailEntity entity) {
		Product prod = new Product();
		prod.setProductId(entity.getProductId());
		prod.setProductName(entity.getProductName());
		prod.setProductModel(entity.getProductModel());
		prod.setCategory(entity.getCategory());
		prod.setAvailableQuantity(entity.getAvailableQuantity());
		prod.setPrice(entity.getPrice());
		return prod;
	}

	private List<Product> mapListEntityToDomain(List<ProductsDetailEntity> listentity) {
		List<Product> listProd = new ArrayList<>();
		for (ProductsDetailEntity entity : listentity) {
			Product prod = new Product();
			prod.setProductId(entity.getProductId());
			prod.setProductName(entity.getProductName());
			prod.setProductModel(entity.getProductModel());
			prod.setCategory(entity.getCategory());
			prod.setAvailableQuantity(entity.getAvailableQuantity());
			prod.setPrice(entity.getPrice());
			listProd.add(prod);
		}

		return listProd;
	}

	@Override
	public List<Product> findAll() throws Exception {
		// TODO Auto-generated method stub
		productsDetailEntity2 = productDetailsRepository.findAll();
		return mapListEntityToDomain(productsDetailEntity2);
	}
}
