package com.telstra.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.telstra.model.Product;

@Service
public interface ProductService {

	public Product fetchProductDetails(String prodid) throws Exception;
	
	public List<Product> findAll() throws Exception;
}
