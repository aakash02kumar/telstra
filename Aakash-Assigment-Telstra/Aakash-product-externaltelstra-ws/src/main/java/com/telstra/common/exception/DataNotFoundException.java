package com.telstra.common.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * This exception class denotes the exceptional case when requested data does
 * not exist in the system.
 *
 */
public class DataNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	private List<ErrorObject> errors;

	public DataNotFoundException(String code, String message) {
		super(message);
		ErrorObject errObject = new ErrorObject(code, message);
		this.addError(errObject);
	}

	public DataNotFoundException(String message, List<ErrorObject> errors) {
		super(message);
		this.setErrors(errors);
	}

	public DataNotFoundException(String message, ErrorObject error) {
		super(message);
		this.addError(error);
	}

	public List<ErrorObject> getErrors() {
		return errors;
	}

	public void setErrors(List<ErrorObject> errors) {
		this.errors = errors;
	}

	public void addError(ErrorObject error) {
		if (this.errors == null) {
			this.errors = new ArrayList<>();
		}
		this.errors.add(error);
	}
}
